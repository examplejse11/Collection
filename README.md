# Collection
Коллекции и дженерики 

#Содержание файла "Track.java"

public class Track {

	public Track() {
		
	}

}

#Содержание файла "CompactDisc.java"

public class CompactDisc extends Media {
	
	public CompactDisc() {
	
	}
	
}

#Содержание файла "Library.java"

public class Library<T> {

	public Library() {
		
	}

}



#Инструкция по запуску проекта

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "javac -sourcepath /projects/Exercise6/Collection/src -d /projects/Exercise6/Collection/bin /projects/Exercise6/Collection/src/*.java && java -classpath /projects/Exercise6/Collection/bin OnlineMedia",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise6/Collection/src",
                "component": "maven"
            }
        }
    ]
}